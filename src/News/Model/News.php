<?php
namespace Sdk\News\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\TopAbleTrait;

use Sdk\Crew\Model\Crew;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\News\Repository\NewsRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class News implements IObject
{
    use Object;

    const APPLY_STATUS = array(
        'PENDING' => 0,
        'REJECTED' => -2,
        'APPROVED' => 2
    );

    private $id;

    private $title;

    private $source;

    private $newsType;

    private $newsTypeCn;

    private $applyStatus;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->source = '';
        $this->newsType = 0;
        $this->newsTypeCn = '';
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new NewsRepository();
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setContent(array $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : array
    {
        return $this->content;
    }

    public function setSource(string $source) : void
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setNewsType(int $newsType) : void
    {
        $this->newsType = $newsType;
    }

    public function getNewsType() : int
    {
        return $this->newsType;
    }

    public function setNewsTypeCn(string $newsTypeCn) : void
    {
        $this->newsTypeCn = $newsTypeCn;
    }

    public function getNewsTypeCn() : string
    {
        return $this->newsTypeCn;
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }
}
