<?php
namespace Sdk\News\Controller;

use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\FetchControllerTrait;
use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Interfaces\INull;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\View\Json\NewsListView;
use Sdk\News\View\Json\NewsView;

class NewsFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait;

    protected $repository;
    
    const LIST_SCENE = array(
        'PUBLISH' => 'MA',
        'APPROVE' => 'MQ'
    );

    public function __construct()
    {
        parent::__construct();
        $this->repository = new NewsRepository();
    }

    protected function fetchOneAction(int $id) : bool
    {
        $news = $this->getRepository()
        ->scenario(NewsRepository::FETCH_ONE_MODEL_UN)
        ->fetchOne($id);

        // if ($news instanceof INull) {
        //     Core::setLastError(RESOURCE_NOT_EXIST);
        //     return false;
        // }

        $this->render(new NewsView($news));
        return true;
        
    }

    protected function filterAction() : bool
    {
        $sort = $this->getRequest()->get('sort', array('-updateTime'));
        $newsType = $this->getRequest()->get('newsType', '');
        $newsType = marmot_decode($newsType);

        $title = $this->getRequest()->get('title', array());
        $scene = $this->getRequest()->get('scene', '');
        $scene = marmot_decode($scene);

        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($sort, $title, $scene, $newsType);

        $newsList = array();

        list($count, $newsList) =
            $this->getRepository()->scenario(NewsRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);

        $this->render(new NewsListView($newsList, $count));
        return true;
    }

    protected function filterFormatChange($sort, $title, $scene, $newsType)
    {
        $sort = $sort;
        $filter = array();

        $filter['newsType'] = $newsType;

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if ($scene == self::LIST_SCENE['PUBLISH']) {
            $filter['applyStatus'] = 1;
        }

        return [$filter, $sort];
    }

    protected function getRepository()
    {
        return $this->repository;
    }
}