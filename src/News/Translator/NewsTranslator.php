<?php
namespace Sdk\News\Translator;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;

class NewsTranslator
{
    public function arrayToObject(array $expression, $news = null)
    {
        unset($news);
        unset($expression);
        return new NullNews();
    }

    public function arrayToObjects(array $expression): array
    {
        unset($expression);
        return array();
    }

    protected function getUserGroupTranslator(): UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getNewsCategoryTranslator(): NewsCategoryTranslator
    {
        return new NewsCategoryTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($news, array $keys = array())
    {
        if (!$news instanceof News) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'createTime',
                'updateTime',
                'status',
                'applyStatus',
                'newsType' => [],
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($news->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $news->getTitle();
        }
        if (in_array('source', $keys)) {
            $expression['source'] = $news->getSource();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $news->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $news->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $news->getStatus();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $news->getApplyStatus();
        }
        if (isset($keys['newsType'])) {
            $expression['newsType'] = array(
                'id' => $news->getNewsType(),
                'name' => $news->getNewsTypeCn()
            );
        }

        return $expression;
    }
}
