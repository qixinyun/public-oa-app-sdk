<?php
namespace Sdk\News\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\News\Translator\NewsTranslator;

class NewsListView extends JsonView implements IView
{
    private $news;

    private $count;

    private $translator;

    public function __construct(
        array $news,
        int $count
    ) {
        $this->news = $news;
        $this->count = $count;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNews(): array
    {
        return $this->news;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): NewsTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getNews() as $news) {
            $data[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'source',
                    'updateTime',
                    'status',
                    'applyStatus',
                    'newsType' => [],
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
