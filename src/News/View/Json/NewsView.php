<?php
namespace Sdk\News\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class NewsView extends JsonView implements IView
{
    private $news;

    private $translator;

    public function __construct(News $news)
    {
        $this->news = $news;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNews(): News
    {
        return $this->news;
    }

    protected function getTranslator(): NewsTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getNews()
        );

        // //MOCK
        // $data = array(
        //     "id" => "MA",
        //     "title" => "新闻标题1",
        //     "source" => "新闻来源1",
        //     "updateTime" => 1535447167,
        //     "status" => 0,
        //     "applyStatus" => 0,
        //     "newsType" => [
        //         "id" => 1,
        //         "name" => "领导小组",
        //     ],
        // );

        $this->encode($data);
    }
}
