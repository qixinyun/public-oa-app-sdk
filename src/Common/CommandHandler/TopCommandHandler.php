<?php
namespace Sdk\Common\CommandHandler;

use Sdk\Common\Command\TopCommand;
use Sdk\Common\Model\ITopAble;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class TopCommandHandler implements ICommandHandler
{
    abstract protected function fetchITopObject($id) : ITopAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(TopCommand $command)
    {
        $this->topAble = $this->fetchITopObject($command->id);

        if ($this->topAble->top()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
