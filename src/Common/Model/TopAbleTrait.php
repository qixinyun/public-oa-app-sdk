<?php
namespace Sdk\Common\Model;

use Common\Adapter\ITopAbleAdapter;

use Marmot\Core;

trait TopAbleTrait
{
    public function top() : bool
    {
        // if (!$this->verifyIsOperationAble()) {
        //     return false;
        // }
        $topAdapter = $this->getITopAbleAdapter();
        return $topAdapter->top($this);
    }

    protected function verifyIsOperationAble()
    {
        if (!$this->isOperateAble()) {
            Core::setLastError(PURVIEW_UNDEFINED);
            return false;
        }

        if ($this->isStick()) {
            return false;
        }

        return true;
    }

    public function cancelTop() : bool
    {
        // if (!$this->isOperateAble()) {
        //     Core::setLastError(PURVIEW_UNDEFINED);
        //     return false;
        // }

        // if (!$this->isStick()) {
        //     return false;
        // }
        $topAdapter = $this->getITopAbleAdapter();
        return $topAdapter->cancelTop($this);
    }

    abstract protected function getITopAbleAdapter() : ITopAbleAdapter;

    public function setStick(int $stick)
    {
        $this->stick = in_array($stick, array_values(self::STICK)) ? $stick : self::STICK['DISABLED'];
    }

    public function getStick() : int
    {
        return $this->stick;
    }

    public function isStick() : bool
    {
        return $this->getStick() == self::STICK['ENABLED'];
    }
}
