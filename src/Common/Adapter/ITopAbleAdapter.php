<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\ITopAble;

interface ITopAbleAdapter
{
    public function top(ITopAble $topAbleObject) : bool;

    public function cancelTop(ITopAble $topAbleObject) : bool;
}
