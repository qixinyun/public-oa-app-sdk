<?php
namespace Sdk\Crew\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\Crew\Adapter\Crew\CrewMockAdapter;
use Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;
use Sdk\Crew\Model\Crew;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class CrewRepository extends Repository
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREW_LIST';
    const FETCH_ONE_MODEL_UN = 'CREW_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CrewRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new CrewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function edit(Crew $crew) 
    {
        return $this->getAdapter()->edit($crew);
    }
}
