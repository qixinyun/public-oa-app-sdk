<?php
namespace Sdk\Crew\Adapter\Crew;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;
use Sdk\Crew\Translator\CrewRestfulTranslator;
use Marmot\Core;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use function GuzzleHttp\json_encode;

class CrewRestfulAdapter extends GuzzleAdapter
{
    use FetchAbleRestfulAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREW_LIST'=>[
        ],
        'CREW_FETCH_ONE'=>[
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CrewRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'crews';
    }

    protected function getResource()
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCrew::getInstance());
    }

    public function edit(Crew $crew) : bool
    {
        return $this->editAction($crew);
    }

    protected function editAction(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array('id', 'purviews')
        );

        $this->patch(
            $this->getResource().'/'.$crew->getId().'/configurePurview',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }
}
