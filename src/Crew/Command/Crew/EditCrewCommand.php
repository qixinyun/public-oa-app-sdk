<?php
namespace Sdk\Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class EditCrewCommand implements ICommand
{
    public $purviews;

    public $id;

    public function __construct(
        array $purviews,
        int $id = 0
    ) {
        $this->purviews = $purviews;
        $this->id = $id;
    }
}
