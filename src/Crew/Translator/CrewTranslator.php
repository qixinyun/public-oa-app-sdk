<?php
namespace Sdk\Crew\Translator;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;

class CrewTranslator 
{
    public function arrayToObject(array $expression, $crew = null)
    {
        unset($crew);
        unset($expression);
        return new NullCrew();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'userName',
                'createTime',
                'updateTime',
                'status',
                'purviews',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($crew->getId());
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $crew->getCellphone();
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $crew->getUserName();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $crew->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $crew->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $crew->getStatus();
        }
        if (in_array('purviews', $keys)) {
            $purviews = $crew->getPurviews();
            $expression['purviews'] = array();
            foreach ($purviews as $purview) {
                $expression['purviews'][] = intval($purview);
            }
        }

        return $expression;
    }
}
