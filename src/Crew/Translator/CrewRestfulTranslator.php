<?php
namespace Sdk\Crew\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;

use Marmot\Interfaces\IRestfulTranslator;

class CrewRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $crew = null)
    {
        return $this->translateToObject($expression, $crew);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return new NullCrew();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $crew->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cellphone'])) {
            $crew->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['userName'])) {
            $crew->setUserName($attributes['userName']);
        }
        if (isset($attributes['updateTime'])) {
            $crew->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['createTime'])) {
            $crew->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['purviews'])) {
            $crew->setPurviews($attributes['purviews']);
        }
    
        if (isset($attributes['status'])) {
            $crew->setStatus($attributes['status']);
        }
        
        if (isset($attributes['statusTime'])) {
            $crew->setStatusTime($attributes['statusTime']);
        }

        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'userName',
                'createTime',
                'updateTime',
                'status',
                'purviews'=>[],
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'crews'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $crew->getId();
        }

        $attributes = array();

        if (in_array('purviews', $keys)) {
            $attributes['purviews'] = $crew->getPurviews();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
