<?php
namespace Sdk\Crew\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Crew\Translator\CrewTranslator;

class CrewListView extends JsonView implements IView
{
    private $crew;

    private $count;

    private $translator;

    public function __construct(
        array $crew,
        int $count
    ) {
        $this->crew = $crew;
        $this->count = $count;
        $this->translator = new CrewTranslator();
        parent::__construct();
    }

    protected function getCrew(): array
    {
        return $this->crew;
    }

    protected function getCount(): int
    {
        return $this->count;
    }

    protected function getTranslator(): CrewTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getCrew() as $crew) {
            $data[] = $translator->objectToArray(
                $crew
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
