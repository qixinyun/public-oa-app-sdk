<?php
namespace Sdk\Crew\View\Json;

use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewTranslator;

class CrewView extends JsonView implements IView
{
    private $crew;

    private $translator;

    public function __construct(Crew $crew)
    {
        $this->crew = $crew;
        $this->translator = new CrewTranslator();
        parent::__construct();
    }

    protected function getCrew(): Crew
    {
        return $this->crew;
    }

    protected function getTranslator(): CrewTranslator
    {
        return $this->translator;
    }

    public function display(): void
    {
        $data = array();

        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getCrew()
        );

        

        $this->encode($data);
    }
}
