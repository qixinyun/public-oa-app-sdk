<?php
namespace Sdk\Crew\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Model\IEnableAble;
use Sdk\Crew\Repository\CrewRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Crew implements IObject
{
    use Object;

    private $id;

    private $cellphone;

    private $userName;

    private $purviews;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->cellphone = '';
        $this->userName = '';
        $this->purviews = array();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new CrewRepository();
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setPurviews(array $purviews) : void
    {
        $this->purviews = $purviews;
    }

    public function getPurviews() : array
    {
        return $this->purviews;
    }


    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    public function edit()
    {
        return $this->getRepository()->edit($this);
    }
}
