<?php
namespace Sdk\Crew\Controller;

use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\FetchControllerTrait;
use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\View\Json\ErrorJsonView;
use Marmot\Framework\View\Json\SuccessJsonView;
use Marmot\Framework\View\Template\ErrorTemplateView;
use Marmot\Framework\View\Template\SuccessTemplateView;
use Marmot\Interfaces\INull;
use Sdk\Crew\Command\Crew\EditCrewCommand;
use Sdk\Crew\CommandHandler\Crew\CrewCommandHandlerFactory;
use Sdk\Crew\Repository\CrewRepository;
use Sdk\Crew\View\Json\CrewListView;
use Sdk\Crew\View\Json\CrewView;

class CrewOperationController extends Controller
{
    protected $repository;
    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new CrewCommandHandlerFactory());
        $this->repository = new CrewRepository();
    }

    protected function edit($id) : bool
    {
        $purviews = $this->getRequest()->post('purviews', '');
        $purviews = explode(',', $purviews);

        $id = marmot_decode($id);
        $crew = $this->getRepository()
        ->scenario(CrewRepository::FETCH_ONE_MODEL_UN)
        ->fetchOne($id);

        if ($crew instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        if (is_array($purviews)) {
            $command = new EditCrewCommand($purviews, $id);

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function getCommandBus()
    {
        return $this->commandBus;
    }

    public function displaySuccess()
    {
        $this->render(new SuccessJsonView());
    }

    public function displayError()
    {
        $this->getRequest()->isAjax() ?
        $this->render(new ErrorJsonView()):
        $this->render(new ErrorTemplateView());
    }

    protected function getRepository()
    {
        return $this->repository;
    }
}