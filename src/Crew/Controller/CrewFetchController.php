<?php
namespace Sdk\Crew\Controller;

use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\FetchControllerTrait;
use Marmot\Framework\Classes\Controller;
use Sdk\Crew\Repository\CrewRepository;
use Sdk\Crew\View\Json\CrewListView;
use Sdk\Crew\View\Json\CrewView;

class CrewFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CrewRepository();
    }

    protected function fetchOneAction(int $id) : bool
    {
        $crew = $this->getRepository()
        ->scenario(CrewRepository::FETCH_ONE_MODEL_UN)
        ->fetchOne($id);

        // if ($Crew instanceof INull) {
        //     Core::setLastError(RESOURCE_NOT_EXIST);
        //     return false;
        // }

        $this->render(new CrewView($crew));
        return true;
        
    }

    protected function filterAction() : bool
    {
        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange();

        $crewList = array();

        list($count, $crewList) =
            $this->getRepository()->scenario(CrewRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);

        $this->render(new CrewListView($crewList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = array('-updateTime');
        $filter = array();

        return [$filter, $sort];
    }

    protected function getRepository()
    {
        return $this->repository;
    }
}