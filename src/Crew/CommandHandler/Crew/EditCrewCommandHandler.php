<?php
namespace Sdk\Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Sdk\Crew\Command\Crew\EditCrewCommand;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;
use Sdk\Crew\Repository\CrewRepository;

class EditCrewCommandHandler implements ICommandHandler
{
    private $repository;

    private $crew;

    public function __construct()
    {
        $this->repository = new CrewRepository();
        $this->crew = new NullCrew();
    }

    public function __destruct()
    {
        unset($this->crew);
        unset($this->repository);
    }

    protected function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    protected function fetchCrew($id) : Crew
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        // $this->crew = $this->fetchCrew($command->id);

        $this->crew = new Crew();
        $this->crew->setId($command->id);
        $this->crew->setPurviews($command->purviews);

        return $this->crew->edit();
    }

    // public function getLog() : Log
    // {
    //     return new Log(
    //         ILogable::OPERATION['OPERATION_EDIT'],
    //         ILogable::CATEGORY['CREW'],
    //         $this->getCrew()->getId(),
    //         $this->getCrew()->getRealName()
    //     );
    // }
}
