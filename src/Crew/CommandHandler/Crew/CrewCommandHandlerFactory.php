<?php
namespace Sdk\Crew\CommandHandler\Crew;

use Marmot\Basecode\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class CrewCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Crew\Command\Crew\EditCrewCommand'=>'Sdk\Crew\CommandHandler\Crew\EditCrewCommandHandler',
       );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
